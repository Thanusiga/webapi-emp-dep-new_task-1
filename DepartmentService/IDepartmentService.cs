﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Models;

namespace WebAPI.DepartmentService
{
    interface IDepartmentService
    {
        Department AddDepartment(Department department);

        List<Department> GetDepartments();

        void UpdateDepartment(Department department);

        void DeleteDepartment(int DepartmentId);

        Department GetDepartment(int DepartmentId);
    }
}
