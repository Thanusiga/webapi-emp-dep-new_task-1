﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.employeeService;
using WebAPI.Models;

namespace WebAPI.employeeService
{
    public class EmployeeService : IEmployeeService
    {
        public EmployeeDbContext _employeeDbContext;

        public EmployeeService(EmployeeDbContext employeeDbContext)
        {
            _employeeDbContext = employeeDbContext;
        }

        public Employee AddEmployee(Employee employee)
        {
            _employeeDbContext.Employees.Add(employee);
            _employeeDbContext.SaveChanges();
            return employee;
        }

        

        public void UpdateEmployee(Employee employee)
        {
            _employeeDbContext.Employees.Update(employee);
            _employeeDbContext.SaveChanges();
        }

        public void DeleteEmployee(int EmployeeId)
        {
            var employee = _employeeDbContext.Employees.FirstOrDefault(x => x.EmployeeId == EmployeeId);
            if (employee != null)
            {
                _employeeDbContext.Remove(employee);
                _employeeDbContext.SaveChanges();
            }
        }

        public Employee GetEmployee(int EmployeeId)
        {
            return _employeeDbContext.Employees.FirstOrDefault(x => x.EmployeeId == EmployeeId);
        }

        public List<Employee> GetEmployees()
        {
            return _employeeDbContext.Employees.ToList();
        }
    }
}
