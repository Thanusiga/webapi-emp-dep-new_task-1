﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace WebAPI.Models
{
    public class Department
    {
        [Key]
        public int departmentId { get; set; }

        [Required]
        [Display(Name = "Department Name")]
        public String departmentName { get; set; }
    }
}
